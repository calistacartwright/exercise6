﻿namespace DiceRollApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dieOneLabel = new System.Windows.Forms.Label();
            this.dieTwoLabel = new System.Windows.Forms.Label();
            this.rollDiceButton = new System.Windows.Forms.Button();
            this.continousButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dieOneLabel
            // 
            this.dieOneLabel.BackColor = System.Drawing.Color.White;
            this.dieOneLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dieOneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dieOneLabel.ForeColor = System.Drawing.Color.Black;
            this.dieOneLabel.Location = new System.Drawing.Point(59, 58);
            this.dieOneLabel.Name = "dieOneLabel";
            this.dieOneLabel.Size = new System.Drawing.Size(90, 90);
            this.dieOneLabel.TabIndex = 0;
            this.dieOneLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dieTwoLabel
            // 
            this.dieTwoLabel.BackColor = System.Drawing.Color.White;
            this.dieTwoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dieTwoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dieTwoLabel.ForeColor = System.Drawing.Color.Black;
            this.dieTwoLabel.Location = new System.Drawing.Point(234, 58);
            this.dieTwoLabel.Name = "dieTwoLabel";
            this.dieTwoLabel.Size = new System.Drawing.Size(90, 90);
            this.dieTwoLabel.TabIndex = 1;
            this.dieTwoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rollDiceButton
            // 
            this.rollDiceButton.BackColor = System.Drawing.Color.White;
            this.rollDiceButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rollDiceButton.Location = new System.Drawing.Point(12, 198);
            this.rollDiceButton.Name = "rollDiceButton";
            this.rollDiceButton.Size = new System.Drawing.Size(137, 64);
            this.rollDiceButton.TabIndex = 2;
            this.rollDiceButton.Text = "Roll Once";
            this.rollDiceButton.UseVisualStyleBackColor = false;
            this.rollDiceButton.Click += new System.EventHandler(this.rollDiceButton_Click);
            // 
            // continousButton
            // 
            this.continousButton.BackColor = System.Drawing.Color.White;
            this.continousButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.continousButton.Location = new System.Drawing.Point(234, 198);
            this.continousButton.Name = "continousButton";
            this.continousButton.Size = new System.Drawing.Size(140, 64);
            this.continousButton.TabIndex = 3;
            this.continousButton.Text = "Continous Roll";
            this.continousButton.UseVisualStyleBackColor = false;
            this.continousButton.Click += new System.EventHandler(this.continousButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(386, 331);
            this.Controls.Add(this.continousButton);
            this.Controls.Add(this.rollDiceButton);
            this.Controls.Add(this.dieTwoLabel);
            this.Controls.Add(this.dieOneLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Dice Roll";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label dieOneLabel;
        private System.Windows.Forms.Label dieTwoLabel;
        private System.Windows.Forms.Button rollDiceButton;
        private System.Windows.Forms.Button continousButton;
    }
}

