﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiceRollApplication
{
    //New Dice class created.
    class Dice
    {
        private int sides;
        

       //Constructor that allows a die object to be created with 4-20 sides.
        public Dice(int numOfSides) 
        {
            
            if (numOfSides < 4)
            {
                numOfSides = 4;
            } else if (numOfSides > 20)
            {
                numOfSides = 20;
            }
            sides = numOfSides;
        }

        //Method that allows die objects to be rolled randomly based on the amount of sides; each roll is returned.
        public int rollDice(Random r) 
        {

            int roll = r.Next(1, sides + 1);
            return roll;
        }
    }
}
