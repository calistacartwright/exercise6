﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiceRollApplication
{
    public partial class Form1 : Form
    {
        public int totalRolls = 20;
        public int dieOneValue;
        public int dieTwoValue;

        //Constant integer that defines how many sides the dice objects will have.
        public const int amtOfSides = 6;
      

        public Form1()
        {
            InitializeComponent();
            
        }

        private void rollDiceButton_Click(object sender, EventArgs e)
        {
            //dieOne and dieTwo are Dice objects created by the Dice class.
            Dice dieOne = new Dice(amtOfSides);
            Dice dieTwo = new Dice(amtOfSides);
            Random randomNum = new Random();      
            
            //Calls rollDice method for each die each time the rollDiceButton is clicked.
            totalRolls++;
            dieOneValue = dieOne.rollDice(randomNum);
            dieTwoValue = dieTwo.rollDice(randomNum);

            //Displays each roll on the given die labels after each click of rollDiceButton.
            dieOneLabel.Text = dieOneValue.ToString();
            dieTwoLabel.Text = dieTwoValue.ToString();
            
            //Once snake eyes are rolled, a message box showing the number of rolls it took appears.
            if (dieOneValue == 1 && dieTwoValue == 1) 
            {
                if (totalRolls > 1)
                {
                    MessageBox.Show("It took " + totalRolls.ToString() + " rolls to get snake eyes!");
                    totalRolls = 0;
                }
                else if (totalRolls == 1)
                {
                    MessageBox.Show("It took " + totalRolls.ToString() + " roll to get snake eyes!");
                    totalRolls = 0;
                }
            }
              
            
            


        }

        private void continousButton_Click(object sender, EventArgs e)
        {
            //dieOne and dieTwo are Dice objects created by the Dice class.
            Dice dieOne = new Dice(amtOfSides);
            Dice dieTwo = new Dice(amtOfSides);
            Random randomNum = new Random();
            bool snakeEyes = false;

            //While loop that allows the dice to roll continously as long as snake eyes have not been reached.
            while (!snakeEyes) 
            {
                totalRolls++;
                dieOneValue = dieOne.rollDice(randomNum);
                dieTwoValue = dieTwo.rollDice(randomNum);
                
                dieOneLabel.Text = dieOneValue.ToString();
                dieTwoLabel.Text = dieTwoValue.ToString();
                
                //Allows die labels to update in the application window by forcing the update to occur, and then by putting the thread to sleep.
                Application.DoEvents();
                Thread.Sleep(50);

                //Once snake eyes are rolled, a message box showing the number of rolls it took appears.
                if (dieOneValue == 1 && dieTwoValue == 1)
                {
                    if (totalRolls > 1)
                    {
                        MessageBox.Show("It took " + totalRolls.ToString() + " rolls to get snake eyes!");
                        totalRolls = 0;
                        snakeEyes = true;
                    }
                    else if (totalRolls == 1)
                    {
                        MessageBox.Show("It took " + totalRolls.ToString() + " roll to get snake eyes!");
                        totalRolls = 0;
                        snakeEyes = true;
                    }
                }
            }
        }
    }
}
